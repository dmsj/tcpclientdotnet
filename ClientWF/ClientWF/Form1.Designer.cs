﻿namespace ClientWF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browse_btn = new System.Windows.Forms.Button();
            this.file_label = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.refresh_btn = new System.Windows.Forms.Button();
            this.connection_label = new System.Windows.Forms.Label();
            this.image_panel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // browse_btn
            // 
            this.browse_btn.BackColor = System.Drawing.Color.Black;
            this.browse_btn.ForeColor = System.Drawing.Color.White;
            this.browse_btn.Location = new System.Drawing.Point(50, 21);
            this.browse_btn.Name = "browse_btn";
            this.browse_btn.Size = new System.Drawing.Size(75, 23);
            this.browse_btn.TabIndex = 7;
            this.browse_btn.Text = "Browse";
            this.browse_btn.UseVisualStyleBackColor = false;
            this.browse_btn.Click += new System.EventHandler(this.browse_btn_Click);
            // 
            // file_label
            // 
            this.file_label.AutoSize = true;
            this.file_label.Location = new System.Drawing.Point(47, 47);
            this.file_label.Name = "file_label";
            this.file_label.Size = new System.Drawing.Size(0, 13);
            this.file_label.TabIndex = 8;
            this.file_label.Visible = false;
            // 
            // save_btn
            // 
            this.save_btn.BackColor = System.Drawing.Color.Black;
            this.save_btn.Enabled = false;
            this.save_btn.ForeColor = System.Drawing.Color.White;
            this.save_btn.Location = new System.Drawing.Point(131, 21);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(75, 23);
            this.save_btn.TabIndex = 9;
            this.save_btn.Text = "Save";
            this.save_btn.UseVisualStyleBackColor = false;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // refresh_btn
            // 
            this.refresh_btn.BackColor = System.Drawing.Color.Black;
            this.refresh_btn.ForeColor = System.Drawing.Color.White;
            this.refresh_btn.Location = new System.Drawing.Point(50, 64);
            this.refresh_btn.Name = "refresh_btn";
            this.refresh_btn.Size = new System.Drawing.Size(156, 23);
            this.refresh_btn.TabIndex = 10;
            this.refresh_btn.Text = "Refresh";
            this.refresh_btn.UseVisualStyleBackColor = false;
            this.refresh_btn.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // connection_label
            // 
            this.connection_label.AutoSize = true;
            this.connection_label.Location = new System.Drawing.Point(3, 2);
            this.connection_label.Name = "connection_label";
            this.connection_label.Size = new System.Drawing.Size(0, 13);
            this.connection_label.TabIndex = 11;
            // 
            // image_panel
            // 
            this.image_panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.image_panel.Location = new System.Drawing.Point(0, 92);
            this.image_panel.Name = "image_panel";
            this.image_panel.Size = new System.Drawing.Size(255, 125);
            this.image_panel.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(255, 217);
            this.Controls.Add(this.image_panel);
            this.Controls.Add(this.connection_label);
            this.Controls.Add(this.refresh_btn);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.file_label);
            this.Controls.Add(this.browse_btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "TcpClient";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button browse_btn;
        private System.Windows.Forms.Label file_label;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button refresh_btn;
        private System.Windows.Forms.Label connection_label;
        private System.Windows.Forms.Panel image_panel;
    }
}

