﻿using System.Drawing;
using System.Windows.Forms;

namespace ClientWF.src.Utils
{
    public abstract class StyleUtil
    {
        public static void SetCommonButtonStyle(Button btn)
        {
            btn.FlatStyle = FlatStyle.Flat;
            btn.FlatAppearance.BorderColor = Color.LightCoral;
            btn.FlatAppearance.BorderSize = 1;
            btn.ForeColor = Color.Black;
            btn.BackColor = Color.BlanchedAlmond;
        }
        public static void SetCommonLabelStyle(Label label)
        {
            label.ForeColor = Color.White;
        }
    }
}
