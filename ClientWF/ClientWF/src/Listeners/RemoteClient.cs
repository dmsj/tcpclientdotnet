﻿using System;
using System.Net.Sockets;
using ClientWF.src.Configuration;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Domain.Model;
using Domain.Enums;

namespace ClientWF.src.Listeners
{
    public class RemoteClient
    {
        private static bool reconnecting;
        public static Form1 Form { get; set; }
        public static Command LastCommand { get; set; }
        public static Boolean Connected
        {
            get
            {
                if (Client == null || !Client.Connected)
                {
                    return false;
                }
                return true;
            }
        }
        public static TcpClient Client { get; set; }

        public static void Disconnect()
        {
            if (Client == null || !Client.Connected)
            {
                return;
            }
            // Release the socket.
            Client.Close();
        }

        private static ServerResponse sendCommand(Command command)
        {
            if (!Connected)
            {
                if (!Connect(Config.HOST_IP, Config.PORT))
                {
                    LastCommand = command;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(InitReconnect));
                    return null;
                }
            }
            IFormatter formatter = new BinaryFormatter();
            NetworkStream stream = Client.GetStream();

            // Send the message to the connected TcpServer. 
            formatter.Serialize(stream, command);

            Console.WriteLine("Sent: command {0}", command.ClientCommand.ToString());

            ServerResponse responseCmd = (ServerResponse)formatter.Deserialize(stream);
            stream.Close();
            Disconnect();
            return responseCmd;
        }

        public static ServerResponse SendCommand(ClientCommand command, Object data)
        {
            return sendCommand(new Command() { ClientCommand = command, Data = data });
        }

        public static bool Connect(string ip, int port)
        {
            try
            {
                Client = new TcpClient(ip, port);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }

            return Connected;
        }

        public static void InitReconnect(object Stateinfo)
        {
            Form.UpdateForm(false, true, false, false);
            if (reconnecting)
            {
                return;
            }
            reconnecting = true;
            int counter = 0;
            while (!Connected)
            {
                Connect(Config.HOST_IP, Config.PORT);
                if (!Connected)
                {
                    counter++;
                    if (counter == 5)
                    {
                        Form.UpdateForm(false, false, true, false);
                        counter = 5;
                    }
                    Console.WriteLine("Reconnecting in 5 seconds...");
                    Thread.Sleep(5000);
                    Form.UpdateForm(false, true, false, false);
                }
            }
            reconnecting = false;
            if (LastCommand != null)
            {
                switch (LastCommand.ClientCommand)
                {
                    case ClientCommand.GET_IMAGES:
                        Form.UpdateImageView(sendCommand(LastCommand));
                        break;
                    case ClientCommand.SAVE_IMAGE:
                        sendCommand(LastCommand);
                        Form.UpdateForm(true, false, false, false);
                        break;
                }
                LastCommand = null;
            }
        }

    }
}
