﻿using ClientWF.src.Listeners;
using System.Windows.Forms;
using System;
using Domain.Enums;
using System.Drawing;
using ClientWF.src.Utils;
using Domain.Model;
using System.Collections.Generic;
using ClientWF.src.Configuration;

namespace ClientWF
{
    public partial class Form1 : Form
    {
        public static Image Img { get; set; }
        private static Dictionary<string, Form> formList;
        public static Dictionary<string, Form> FormList { get { if (formList == null) { formList = new Dictionary<string, Form>(); } return formList; } }
        public Form1()
        {
            Config.Load();
            InitializeComponent();
            RemoteClient.Form = this;
            image_panel.HorizontalScroll.Maximum = 0;
            image_panel.AutoScroll = false;
            image_panel.VerticalScroll.Visible = false;
            image_panel.AutoScroll = true;
            updateStyles();
        }

        public void UpdateForm(bool saved, bool reconnecting, bool failed, bool done)
        {
            bool saveBtn;
            if (Img == null)
            {
                saveBtn = false;
            }
            else
            {
                saveBtn = true;
            }
            if (save_btn.InvokeRequired)
            {
                save_btn.Invoke(new MethodInvoker(delegate { save_btn.Enabled = saveBtn; }));
            }
            else
            {
                save_btn.Enabled = saveBtn;
            }
            if (saved)
            {
                if (file_label.InvokeRequired)
                {
                    file_label.Invoke(new MethodInvoker(delegate { file_label.Text = "Saved"; }));
                }
                else
                {
                    file_label.Text = "Saved";
                }
            }
            string connectionStatus = string.Empty;
            Color connectionColor = Color.Empty;
            if (reconnecting)
            {
                connectionStatus = "Connecting...";
                connectionColor = Color.White;
            }
            else if (failed)
            {
                connectionStatus = "Failed";
                connectionColor = Color.Red;
            }
            else if (done)
            {
                connectionStatus = "Done";
                connectionColor = Color.White;
            }
            if (connection_label.InvokeRequired)
            {
                connection_label.Invoke(new MethodInvoker(delegate { connection_label.Text = connectionStatus; connection_label.ForeColor = connectionColor; }));
            }
            else
            {
                connection_label.Text = connectionStatus;
                connection_label.ForeColor = connectionColor;
            }
        }

        private void browse_btn_Click(object sender, EventArgs e)
        {
            file_label.Visible = false;
            Img = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *.bmp, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp, *.png";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Multiselect = false;
            UpdateForm(false, false, false, false);
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Img = Image.FromFile(openFileDialog1.FileName);
                    string name = openFileDialog1.SafeFileName;
                    Img.Tag = name;
                    Console.WriteLine("Image name " + Img.Tag);
                    file_label.Visible = true;
                    file_label.Text = name;
                    UpdateForm(false, false, false, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            if (RemoteClient.SendCommand(ClientCommand.SAVE_IMAGE, new ClientImage() { Name = (string)Img.Tag, Img = ImageUtil.imageToByteArray(Img) }) != null)
            {
                Img = null;
                UpdateForm(true, false, false, false);
            }
        }

        private void btn_show_form_click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Form form;
            bool valid = FormList.TryGetValue(btn.Name, out form);
            if (valid)
            {
                form.ShowDialog();
            }
        }

        public void UpdateImageView(ServerResponse response)
        {
            if (response == null || response.ClientImageList == null)
            {
                UpdateForm(false, false, true, false);
                return;
            }
            FormList.Clear();
            if (image_panel.InvokeRequired)
            {
                image_panel.Invoke(new MethodInvoker(delegate { image_panel.Controls.Clear(); }));
            }
            else
            {
                image_panel.Controls.Clear();
            }
            foreach (ClientImage cImg in response.ClientImageList)
            {
                string imgName = cImg.Name;
                Image img = ImageUtil.byteArrayToImage(cImg.Img);
                Form form;
                bool valid = FormList.TryGetValue(imgName, out form);
                if (valid)
                {
                    int index = 1;
                    imgName = string.Format("{0}{1}", imgName, index);
                    while (FormList.TryGetValue(imgName, out form))
                    {
                        index++;
                        imgName = string.Format("{0}{1}", imgName, index);
                    }
                }
                form = new Form();
                form.Name = imgName;
                form.Width = img.Width + 15;
                form.Height = img.Height + 40;
                PictureBox pb = new PictureBox();
                pb.Image = img;
                pb.Dock = DockStyle.Fill;
                form.Controls.Add(pb);
                FormList.Add(imgName, form);
                Button btn = new Button();
                btn.Text = cImg.Name;
                btn.Name = imgName;
                btn.Click += new EventHandler(btn_show_form_click);
                btn.Dock = DockStyle.Top;
                StyleUtil.SetCommonButtonStyle(btn);
                if (InvokeRequired)
                {
                    Invoke(new MethodInvoker(delegate { image_panel.Controls.Add(btn); }));
                }
                else
                {
                    image_panel.Controls.Add(btn);
                }
            }
            UpdateForm(false, false, false, true);
        }
        private void refresh_btn_Click(object sender, EventArgs e)
        {
            UpdateImageView(RemoteClient.SendCommand(ClientCommand.GET_IMAGES, null));
        }
        private void updateStyles()
        {
            foreach (Control c in this.Controls)
            {
                if (c is Button)
                {
                    StyleUtil.SetCommonButtonStyle((Button)c);
                }
                else if (c is Label)
                {
                    StyleUtil.SetCommonLabelStyle((Label)c);
                }
            }
        }
    }
}
