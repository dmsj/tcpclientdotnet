﻿namespace Domain.Enums
{
    public enum ClientCommand
    {
        NONE, SAVE_IMAGE, GET_IMAGES
    }
}
