﻿using Domain.Enums;
using System;
using System.Collections.Generic;

namespace Domain.Model
{
    [Serializable]
    public class ServerResponse
    {
        public ClientCommand ClientCommand { get; set; }
        public IEnumerable<ClientImage> ClientImageList { get; set; }
    }
}
