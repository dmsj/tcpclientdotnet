﻿using Domain.Enums;
using System;

namespace Domain.Model
{
    [Serializable]
    public class Command
    {
        public ClientCommand ClientCommand { get; set; }
        public Object Data { get; set; }
    }
}
