﻿using System;

namespace Domain.Model
{
    [Serializable]
    public class ClientImage
    {
        public virtual long ID { get; set; }
        public virtual string Name { get; set; }
        public virtual byte[] Img { get; set; }
    }
}
